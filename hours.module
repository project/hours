<?php 
/**
 * Provides the biography content type.
 * @file
 */
/**
 * Implementation of hook_help().
 */
function hours_help($path, $arg) {

  if ($path == 'admin/help#hours') {
    $txt = '\'hours\' is a content type meant to be used primarily by independent '
      .' contractors.  The \'hours\' type represents a time entry. '
      .'\'hours\' are useful for keeping track of billable hours, '
      .'and can be used in conjunction with the invoice module '
      .'to generate invoices.';
    return '<p>'. t($txt) .'</p>';
  }
}

/**
 * Implements hook_perm()
 */
function hours_perm() {
  return array(
    'create time entry nodes',
    'edit own time entry nodes',
    'delete own time entry nodes',
    'send time entry nodes',
    'administer hours',
  );
}

/**
 * Implements hook_access()
 */
function hours_access($op, $node, $account) {
  if (user_access('administer hours', $account)) {
    return TRUE;
  }
  
  switch ($op) {
    case 'create':
      return user_access('create time entry nodes', $account);
    
    case 'view':
      if ($node->uid == $account->uid && user_access('create time entry nodes', $account)) {
        return TRUE;
      }
    case 'update':
      if ($node->uid == $account->uid && user_access('edit own time entry nodes', $account)) {
        // Check to see if this node has been submitted for admin approval.
        $result = db_query("SELECT * from {hours_sent} WHERE nid = %d", $node->nid);
        if ($row = db_fetch_object($result)) {
          // If the node has been submitted for admin approval, then you'll need to be an hours admin 
          // in order to edit it.
          if (user_access('administer hours', $account)) {
            return TRUE;
          }
          else {
            return FALSE;
          }
        }
        return TRUE;
      }
    case 'delete':
      // If you own this node, and you have the perms to delete nodes you own...
      if ($node->uid == $account->uid && user_access('delete own time entry nodes', $account)) {
        // Check to see if this node has been submitted for admin approval.
        $result = db_query("SELECT * from {hours_sent} WHERE nid = %d", $node->nid);
        if ($row = db_fetch_object($result)) {
          // If the node has been submitted for admin approval, then you'll need to be an hours admin
          // in order to delete it.
          if (user_access('administer hours', $account)) {
            return TRUE;
          }
          else {
            // Otherwise, you're out of luck.
            return FALSE;
          }
        }
        return TRUE;
      }
    default:
      return FALSE;
  }
}

function _has_been_submitted($nid) {

  $count = db_fetch_object(db_query("SELECT count(*) as c FROM {hours_sent} WHERE nid = %d", $nid));
  return $count->c;
}

function hours_node_load($nid) {
  global $user;
  $node = node_load($nid);
  if (($node->uid == $user->uid && user_access('send time entry nodes')) || user_access('administer hours')) {
    if ($node->type == 'hours') {
      return $node;
    }
  }
  return FALSE;
}

function submit_hours_tab() {
  global $base_url;
  
  // Make sure that the path is 'node/<nid>/submit_hours.
  // This tells us that we are on the 'Submit Hours' tab.
  // If we aren't for some reason, then return FALSE.
  if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == 'submit_hours') {
    $node = node_load(arg(1));
  }
  else {
    return FALSE;
  }
  
  
  // Set up the output.
  $output = "";
  
  $single_url = $base_url .'/hours/send/'. $node->nid;
  $batch_url = $base_url .'/hours/send';

  $output .= theme('hours_submission', $single_url, $batch_url);
  return $output;
}

/**
 * Implementation of hook_menu().
 */
function hours_menu() {
  $items = array();

  $items['node/%hours_node/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  
  $items['node/%hours_node/submit_hours'] = array(
    'title' => t('Submit Hours'),
    'page callback' => 'submit_hours_tab',
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
  );
  
  $items['admin/hours/settings'] = array(
    'title' => t('Hours Settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hours_admin_settings'),
    'access arguments' => array('administer hours'),
  );

  $items['admin/hours/list/%'] = array(
    'title' => t('Hours Administration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hours_admin_hours'),
    'access arguments' => array('administer hours'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/hours/list'] = array(
    'title' => t('Hours Administration'),
    'description' => "Administer hours",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hours_admin_users'),
    'access arguments' => array('administer hours'),
    'type' => MENU_CALLBACK,
  );
  
  $items['hours/send'] = array(
    'title' => t('Send Hours'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hours_send'),
    'access arguments' => array('send time entry nodes'),
    'type' => MENU_CALLBACK,
  );
  $items['hours/send/%hours_node'] = array(
    'title' => t('Send Hours'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hours_send'),
    'access arguments' => array('send time entry nodes'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}
function hours_admin_users($form_state) {
  $form['admin']  = hours_admin_users_list();
  $form['#theme'] = 'node_filter_form';
  return $form;
}

function hours_admin_users_list() {

  $result = pager_query(db_rewrite_sql("SELECT u.* FROM {users} u ORDER BY u.name ASC"), 50, 0, NULL);

  $languages = language_list();
  $destination = drupal_get_destination();
  $users = array();

  while ($user = db_fetch_object($result)) {
    $account = user_load($user->uid);
    // Don't list the user, unless they have the ability to create time entries
    if (user_access('create time entry nodes', $account)) {
      $users[$user->uid] = '';
      $form['title'][$user->uid] = array('#value' => l($user->name, 'admin/hours/list/'. $user->uid));
      if (module_exists('invoice')) {
        $form['operations'][$user->uid] = array('#value' => l(t('view invoices'), 'admin/invoice/list/'. $user->uid, array('query' => $destination)));
      }
      else {
        $form['operations'][$user->uid] = array('#value' => 'To view invoices for this user, install the invoice module.');
      }
    }                                      
  }
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  $form['#theme'] = 'hours_admin_users';
  return $form;
}

function hours_admin_hours($form_state) {
  $form['admin']  = hours_admin_hours_list();
  $form['#theme'] = 'node_filter_form';
  return $form;
}

function hours_admin_hours_list() {

  $result = pager_query(db_rewrite_sql("SELECT n.*, u.name FROM {node} n INNER JOIN {users} u ON n.uid = u.uid INNER JOIN {hours_sent} hs ON hs.nid = n.nid INNER JOIN {hours} h ON h.nid = hs.nid WHERE n.type = 'hours' AND u.uid = %d ORDER BY u.name ASC"), 50, 0, NULL, array(arg(3)));

  $languages = language_list();
  $destination = drupal_get_destination();
  $nodes = array();
  while ($node = db_fetch_object($result)) {
    $nodes[$node->nid] = '';
    $options = empty($node->language) ? array() : array('language' => $languages[$node->language]);
    $form['title'][$node->nid] = array('#value' => l($node->title, 'node/'. $node->nid, $options) .' '. theme('mark', node_mark($node->nid, $node->changed)));
    $form['username'][$node->nid] = array('#value' => theme('username', $node));
    if ($multilanguage) {
      $form['language'][$node->nid] = array('#value' => empty($node->language) ? t('Language neutral') : t($languages[$node->language]->name));
    }
    $form['operations'][$node->nid] = array('#value' => l(t('edit'), 'node/'. $node->nid .'/edit', array('query' => $destination)));
  }
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  $form['#theme'] = 'hours_admin_hours';
  return $form;
}

function hours_send() {
  global $user;
  
  $sent = array();
  $to_send = array();
  
  $datetime = time();
  $date = date('Y-m-d');
  
  // Build an array of previously sent hours
  $result = db_query("SELECT nid FROM {hours_sent}");
  while ($row = db_fetch_object($result)) {
    $sent[] = $row->nid;
  }
  if (arg(0) == 'hours' && arg(1) == 'send' && is_numeric(arg(2))) {
    // Only sending one time entry...
    // Get only *this* time entry.
    $result = db_query("SELECT * FROM {node} n LEFT JOIN {node_revisions} nr ON n.vid = nr.vid LEFT JOIN {hours} h ON n.vid = h.vid WHERE n.nid = %d", arg(2));
  }
  else {
    // Sending ALL unsent time entries for this user
    // Get all the time entries (hours) for this user
    $result = db_query("SELECT * FROM {node} n LEFT JOIN {node_revisions} nr ON n.vid = nr.vid LEFT JOIN {hours} h ON n.vid = h.vid WHERE n.type = 'hours' AND n.uid = %d", $user->uid); 
 }

  while ($row = db_fetch_object($result)) {
    // If the current time entry has not already been sent...
    if (!in_array($row->nid, $sent)) {
      // Insert it into the hours_sent table
      $insert_result = db_query("INSERT INTO {hours_sent} (vid, nid, date_sent) VALUES (%d, %d, '%s')", $row->vid, $row->nid, $date);

      // Build the string (in case of email send)     
      $str = $row->date ."     ". $row->entry ."     ". $row->client ."     ". $row->task ."\n";
      $to_send[] = $str;
    }
  }
  // Send the hours
  if (count($to_send) != 0) {
    // If the email method has been selected in admin/hours/settings, then email these hours
    if (variable_get('send_method', 'onsite') == 'email') {
      $payable_email = variable_get('payable_email', "");
      if ($payable_email != '') {
        drupal_mail('hours_send', 'time_entries', $payable_email, user_preferred_language($user), $to_send, 'Hours Module', TRUE);
      }
    }
    else {
      if (arg(0) == 'hours' && arg(1) == 'send' && is_numeric(arg(2))) {
        // Only sent one
        drupal_set_message("Your time entry is now available for viewing by the hours admins.");
      }
      else {
        // Sent all of them
        drupal_set_message("Your time entries are now available for viewing by the hours admins.");
      }
    }
  }
  else {
    if (arg(0) == 'hours' && arg(1) == 'send' && is_numeric(arg(2))) {
      // Tried to send one of them
      drupal_set_message("This time entry is already up-to-date.");
    }
    else {
      // Tried to send all of them
      drupal_set_message("Your time entries are already up-to-date.");
    }
  }
}

function hours_send_mail ($key, &$message, $params) {
  global $user;
  
  switch($key) {
    case 'time_entries':
      $message['subject'] = t('Time Entries from '. $user->name);
      foreach ($params as $key => $value) {
        $message['body'] .= $value;
      }
      break;
  }
  
  drupal_set_message("Your time entries have been sent to ". variable_get(payable_email, " ") .".");
}

function hours_admin_settings($context) {
  
  global $user;
  $form = array();
  
  $account = user_load($user->uid);
  
  // User info
  $form['account'] = array(
    '#type' => 'value',
    '#value' => $account,
  );
  
  $form['send_method'] = array(
    '#type' => 'radios',
    '#title' => t('Send Method'),
    '#options' => array('onsite' => 'On Site', 'email' => 'Email'),
    '#default_value' => 'onsite',
    '#description' => t('Choose email in order to send your time entries off-site via email to the address entered below, as well as have them available onsite to hours admins.'),
  );
  
  $form['payable_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Payable Email'),
    '#size' => 65,
    '#maxlength' => 255,
    '#default_value' => variable_get('payable_email', ''),
    '#description' => t('This is the email address that your hours should be sent to.  Make sure you enter it correctly.'),
  );
  
  return system_settings_form($form);
}

/**
 * Implementation hook_node_info().
 */
function hours_node_info() {
  return array(
    'hours' => array(
      'name' => t('Hours'),
      'module' => 'hours',
      'description' => t('A time entry.'),
      'has_title' => FALSE,
      'has_body' => TRUE,
      'body_label' => t('Task'),
      'locked' => TRUE,
    )
  );
}

/**
 * Helper function to convert hh:mm to h.mm and vice versa
 */
function _time_entry_converter($entry) {
  if (strstr($entry, '.')) {
    // Convert to hh:mm
    $entry = explode('.', $entry);
    if ($entry[1] != '') {
      // Since I've used the '.' to separate the float tranches, I need
      // to put it back in order for multiply by 60 to yield the proper
      // results
      $entry[1] = '.'. $entry[1];
      $entry[1] *= 60;
    }
    if ($entry[0]) {
      $entry = implode(':', $entry);
    }
    else {
      $entry = ':'. $entry[1];
    }
    return $entry;
  }
  else if (strstr($entry, ':')) {
    // Convert to h.mm
    
    $float_entry = 0;
    $entry_tranches = explode(':', $entry);
    if ($entry_tranches[1] != '') {
      $float_entry = ($entry_tranches[1]/60);
    }
    if ($entry_tranches[0] != '') {
      $float_entry += $entry_tranches[0];
    }
    // Set the node's entry to the new float value
    return $float_entry;
  }
  else {
    // Something went wrong.  The data that was passed in was
    // not valid.
    return FALSE;
  }
  
}

/**
 * Implementation of hook_form().
 */
function hours_form(&$node) {
  if (isset($node->entry)) {
    $node->entry = _time_entry_converter($node->entry);
  }
  
  $type = node_get_types('type', $node);
  // Existing fields: title and body
  if ($type->has_title) {
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => check_plain($type->title_label),
      '#required' => FALSE,
      '#default_value' => $node->title,
      '#weight' => -5,
    );
  }
  if ($type->has_body) {
    $form['body_field'] = node_body_field(
      $node,
      $type->body_label,
      $type->min_word_count
    );
  }
  // Custom fields
  $form['date_field'] = array(
    '#type' => 'date_popup',
    '#title' => t('Date'),
    '#description' => t('The date the work was done on.'),
    '#date_format' => 'm-d-Y',
    '#default_value' => isset($node->date_field) ? $node->date_field : '',
    '#weight' => -10,
  );
  $form['client'] = array( 
    '#type' => 'textfield',
    '#title' => t('Client'),
    '#description' => t('Client to bill this time to.'),
    '#default_value' => isset($node->client) ? $node->client : '',
    '#weight' => -8,
  );
  $form['entry'] = array(
    '#type' => 'textfield',
    '#title' => t('Hours'),
    '#size' => 5,
    '#description' => t('Number of hours worked. In the form hh:mm.'),
    '#default_value' => isset($node->entry) ? $node->entry : '',
    '#weight' => -9,
  );
  
  return $form;
}

/**
 * Implementation of hook_insert().
 */
function hours_insert($node) {
  if (!isset($node->date_field)) {
    $node->date_field = '';
  }
  if (!isset($node->client)) {
    $node->client = '';
  }
  if (!isset($node->entry)) {
    $node->entry = '';
  }

  db_query(
    'INSERT INTO {hours} (vid, nid, date, client, entry) '
      ."VALUES (%d, %d, '%s', '%s','%f')",
    $node->vid,
    $node->nid,
    $node->date_field,
    $node->client,
    $node->entry
  );
}

/**
 * Implementation of hook_update().
 */
function hours_update($node) {
  if ($node->revision) {
    hours_insert($node);
  }
  else {
    db_query("UPDATE {hours} "
        ."SET date = '%s', client = '%s', entry='%f' "
        ."WHERE vid = %d",
      $node->date_field,
      $node->client,
      $node->entry,
      $node->vid
    );
  }
}

/**
 * Implementation of hook_delete().
 */
function hours_delete($node) {
  global $user;
  
  if (!user_access('administer hours', $user)) {
    db_query('DELETE FROM {hours_sent} WHERE nid = %d', $node->nid);
    db_query('DELETE FROM {hours} WHERE nid = %d', $node->nid);
  }
  else {
    drupal_set_message("You don't have permission to delete time entries once they have been submitted to the hours admins.", 'error');
  }
}

/**
 * This handles the deleting of node revisions AND the automatic saving of the title.
 * Implementation of hook_nodeapi().
 */
function hours_nodeapi(&$node, $op, $teaser, $page) {
  // In the case that this install of drupal is in a subfolder (which it shouldn't be, but...)
  // then I will need the $base_url to construct the url for sending hours.  See the 'view'
  // case below.
  global $base_url;
  
  switch ($op) {
    case 'presave':
      if ($node->type != 'hours') {
        break;
      }
      //TODO: Find a way to disable comments without this hack
      $node->comment = 0;
      $node->title = $node->entry .' - '. $node->client .' - '. $node->body;

      $node->entry = _time_entry_converter($node->entry);
      break;
    case 'delete revision':
      db_query('DELETE FROM {hours} WHERE vid = %d', $node->vid);
      break;
  }
}

/**
 * Implementation of hook_load().
 */
function hours_load($node) {
  $result = db_query('SELECT date as date_field, client, entry FROM {hours} WHERE vid = %d', $node->vid);
  return db_fetch_object($result);
}

/**
 * Implementation of hook_view().
 */
function hours_view($node, $teaser = FALSE, $page = FALSE) {
  // We want the entire node to be themed by theme_time_entry() --
  // including the body, so instead of calling node_prepare to
  // put the body into the content array, I'll do that myself.
  // NOTE:  Be careful here.  Since we don't want to allow any
  // markup in any of the hours fields, make sure to use a call to 
  // check_plain() on each field for security purposes. (prevent xss attacks)
  $body = check_plain($node->body);
  $date = check_plain($node->date_field);
  $client = check_plain($node->client);
  $entry = check_plain($node->entry); 
  $date = date('m/d/Y', strtotime($date));
  
  // I want to theme the time_entry slightly differently, depending on whether it
  // has been submitted to the hours admins.  Since I don't want that logic in my theme
  // function, I have to do it here...
  $is_submitted = FALSE;
  $result = db_query("SELECT * FROM {hours_sent} WHERE nid = %d", $node->nid);
  if ($row = db_fetch_object($result)) {
    $is_submitted = TRUE;
  }
  
  $node->content['time_entry'] = array(
    '#value' => theme('time_entry', $body, $date, $client, $entry, $is_submitted),
    '#weight' => 1,
  );
  
  return $node;
}

/**
 * Theme functions
 */
function theme_time_entry($body, $date, $client, $entry, $submitted_to_admins) {
  
  $output = "";
  
  if ($submitted_to_admins) {
    global $base_url;
    
    $module_path = drupal_get_path('module', 'hours');
    $img_path = "/". $module_path . "/images/submitted_to_admins.png";
    $output .= '<div style="background: url('. $base_url . $img_path .') no-repeat 20% 0">';
  }
  else {
    $output .= '<div>';
  }
  
  $output .= '<p>'. $date .'</p>';
  $output .= '<p>'. $entry .' - '. $client .' - '. $body .'</p></div>';
  
  return $output;
}

function theme_hours_submission($single_url, $batch_url) {
  $output = "";
  
  $output .= '<a href="'. $single_url .'">Submit Just This Entry</a>';
  $output .= '<br /><br />';
  $output .= '<a href="'. $batch_url .'">Submit All Entries</a>';

  return $output;
}

function theme_hours_admin($uid, $body, $date, $client, $entry) {
  global $base_url;
  
  static $last_user;
  $user = user_load($uid);
  $name = $user->name;
  
  $output = "";
  
  if($uid != $last_user) {
    $output .= '<hr><div style="float: left"><span style="font-size: 1.2em"><a href="'. $base_url .'/admin/hours/'. $uid .'">'. $name ."'s hours</a></span></div>";
    if (module_exists('invoice')) {
      $output .= '<div style="float: right;"><a href="'. $base_url .'/admin/invoice/list/'. $uid .'">INVOICES</a></div>';
    }
    $output .= '<hr style="clear: both">';
  }
  
  $output .= '<p><strong>'. $date .'</strong> - '. $name .' - '. $entry .' - '. $client .' - '. $body .'</p></style>';

  $last_user = $uid;
  return $output;
}

function theme_hours_admin_users($form) {
  // If there are rows in this form, then $form['title'] contains a list of
  // the title form elements.
  $has_posts = isset($form['title']) && is_array($form['title']);
  $select_header = $has_posts ? theme('table_select_header_cell') : '';
  $header = array(t('User'), t('Operations'));

  $output = '';

  if ($has_posts) {
    foreach (element_children($form['title']) as $key) {
      $row = array();
      $row[] = drupal_render($form['title'][$key]);
      $row[] = drupal_render($form['operations'][$key]);
      $rows[] = $row;
    }

  }
  else {
    $rows[] = array(array('data' => t('No hours have been submitted by this user.'), 'colspan' => '6'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

function theme_hours_admin_hours($form) {
  // If there are rows in this form, then $form['title'] contains a list of
  // the title form elements.
  $has_posts = isset($form['title']) && is_array($form['title']);
  $select_header = $has_posts ? theme('table_select_header_cell') : '';
  $header = array(t('Title'), t('User'));
  if (isset($form['language'])) {
    $header[] = t('Language');
  }
  $header[] = t('Operations');
  $output = '';

  if ($has_posts) {
    foreach (element_children($form['title']) as $key) {
      $row = array();
      $row[] = drupal_render($form['title'][$key]);
      $row[] = drupal_render($form['username'][$key]);
      if (isset($form['language'])) {
        $row[] = drupal_render($form['language'][$key]);
      }
      $row[] = drupal_render($form['operations'][$key]);
      $rows[] = $row;
    }

  }
  else {
    $rows[] = array(array('data' => t('No hours have been submitted by this user.'), 'colspan' => '6'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

/**
 * Implementation of hook_theme().
 */
function hours_theme() {
  return array(
    'hours_admin' => array(
      'arguments' => array(
        'uid' => NULL,
        'body' => NULL,
        'date' => NULL,
        'client' => NULL,
        'entry' => NULL,
      ),
    ),
    
    'hours_submission' => array(
      'arguments' => array(
        'single_url' => '#',
        'batch_url' => '#',
      ),
    ),

    'hours_admin_hours' => array(
      'arguments' => array(
        'form' => NULL,
      ),
    ),

    'hours_admin_users' => array(
      'arguments' => array(
        'form' => NULL,
      ),
    ),
    
    'time_entry' => array(
      'arguments' => array(
        'body' => NULL,
        'date' => NULL,
        'client' => NULL,
        'entry' => NULL,
        'submitted_to_admins' => NULL,
      ),
    ),
  );
}